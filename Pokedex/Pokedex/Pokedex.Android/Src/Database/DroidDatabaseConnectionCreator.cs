﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Pokedex.Droid.Src.Database;
using Pokedex.Src.Database;
using SQLite;
using Xamarin.Forms;

[assembly: Dependency(typeof(DroidDatabaseConnectionCreator))]
namespace Pokedex.Droid.Src.Database
{
    public class DroidDatabaseConnectionCreator : DatabaseConnectionCreator
    {
        protected override string GetPathToDatabase() {
            return Path.Combine(base.GetPersonalFolder(), Settings.DATABASE_FILE_FULL_NAME);
        }

        protected override void CopyPreCreatedDatabase(string path) {
            using (var binaryReader = new BinaryReader(Android.App.Application.Context.Assets.Open(Settings.DATABASE_FILE_FULL_NAME))) {
                using (var binaryWriter = new BinaryWriter(new FileStream(path, FileMode.Create))) {
                    byte[] buffer = new byte[2048];
                    int length = 0;
                    while ((length = binaryReader.Read(buffer, 0, buffer.Length)) > 0) {
                        binaryWriter.Write(buffer, 0, length);
                    }
                }
            }
        }
    }
}