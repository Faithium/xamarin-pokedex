﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Foundation;
using Pokedex.iOS.Src.Database;
using SQLite;
using UIKit;
using Xamarin.Forms;
using Pokedex.Src.Database;

[assembly: Dependency(typeof(IOSDatabaseConnectionCreator))]
namespace Pokedex.iOS.Src.Database
{
    public class IOSDatabaseConnectionCreator : DatabaseConnectionCreator
    {
        public override SQLiteConnection CreateConnection() {
            var libFolder = GetLibFolder();
            if (!Directory.Exists(libFolder)) {
                Directory.CreateDirectory(libFolder);
            }
            return base.CreateConnection();
        }

        private string GetLibFolder() {
            return Path.Combine(base.GetPersonalFolder(), "..", "Library", "Database");
        }

        protected override string GetPathToDatabase() {
            return Path.Combine(this.GetLibFolder(), Settings.DATABASE_FILE_FULL_NAME);
        }

        protected override void CopyPreCreatedDatabase(string path) {
            var existingDb = NSBundle.MainBundle.PathForResource(Settings.DATABASE_FILE_NAME, Settings.DATABASE_FILE_TYPE);
            File.Copy(existingDb, path);
        }
    }
}