﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using Pokedex.iOS.Src.Renderer;
using Pokedex.Src.Renderer;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;


[assembly: ExportRenderer(typeof(CustomButton), typeof(IOSCustomButton))]
namespace Pokedex.iOS.Src.Renderer
{
    public class IOSCustomButton : ButtonRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Button> e) {
            base.OnElementChanged(e);

            if (e.OldElement == null) {
                Control.Layer.CornerRadius = 60f;
                Control.Layer.BorderWidth = 5f;
                Control.Layer.BorderColor = Color.Black.ToCGColor();
                Control.Layer.BackgroundColor = new Color(33, 150, 243).ToCGColor();
            }
        }
    }
}