﻿using Pokedex.Src.Service;
using Pokedex.ViewModels;
using Xamarin.Forms;

namespace Pokedex.Views
{
    public partial class PokemonDetail : ContentPage
    {
        public int PokemonId { get; }

        public PokemonDetail(ushort pokemonId, string pokemonName) {
            InitializeComponent();
            ((PokemonDetailViewModel) base.BindingContext).Pokemon = PokemonService.GetPokemon(pokemonId.ToString(), pokemonName, true);
        }

        private void HideAll() {
            this.overview.IsVisible = false;
            this.learnset.IsVisible = false;
        }

        private void ShowOverview(object sender, System.EventArgs e) {
            this.HideAll();
            this.overview.IsVisible = true;
        }

        private void ShowLearnset(object sender, System.EventArgs e) {
            this.HideAll();
            this.learnset.IsVisible = true;
        }
    }
}
