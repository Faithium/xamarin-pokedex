﻿using Pokedex.Src.Model.Pokemon;
using Pokedex.Src.Service;
using Pokedex.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Pokedex.Views
{
    public partial class PokemonList : ContentPage
    {
        private List<Pokemon> allPokemons = new List<Pokemon>();

        public PokemonList()
        {
            InitializeComponent();
            new Task(() => {
                this.allPokemons = PokemonService.GetPokemons();
                this.UpdatePokemonList();
            }).Start();
            clearButton.GestureRecognizers.Add(new TapGestureRecognizer() {
                Command = new Command(() => this.ClearSearchByNameText())
            });
        }

        public async void GoToPokemonDetail(object sender, EventArgs eventArgs) {
            try {
                ListView pokemonListView = sender as ListView;
                Pokemon pokemon = pokemonListView.SelectedItem as Pokemon;
                await Navigation.PushAsync(new PokemonDetail(pokemon.Id, pokemon.Name));
            }
            catch (Exception exception) {
                Console.WriteLine(exception.Message);
            }
        }

        private void UpdatePokemonList() {
            List<Pokemon> searchedPokemons = this.allPokemons.FindAll(pokemon => pokemon.Name.ToLower().Contains(searchByName.Text.ToLower()));
            ((PokemonListViewModel) base.BindingContext).Pokemons = new ObservableCollection<Pokemon>(searchedPokemons);
        }

        private void SearchByName_TextChanged(object sender, TextChangedEventArgs e) {
            UpdatePokemonList();
        }

        private void ClearSearchByNameText() {
            searchByName.Text = "";
        }
    }
}
