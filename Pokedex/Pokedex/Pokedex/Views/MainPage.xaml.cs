﻿using System;
using Xamarin.Forms;

namespace Pokedex.Views
{
    public partial class MainPage : ContentPage
    {
        public MainPage() {
            InitializeComponent();
        }

        public async void GoToPokemonList(object sender, EventArgs eventArgs) {
            await Navigation.PushAsync(new PokemonList());
        }
    }
}