﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pokedex.Src.Util
{
    public class EnumUtils
    {
        public static T Parse<T>(string name, bool ignoreCase = false) {
            try {
                return (T) Enum.Parse(typeof(T), name, ignoreCase);
            }
            catch (Exception exception) {
                Console.WriteLine(exception.Message);
                return default(T);
            }
        }
    }
}
