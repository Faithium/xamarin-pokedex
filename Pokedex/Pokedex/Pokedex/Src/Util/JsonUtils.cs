﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Pokedex.Src;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace Pokedex.Src.Util
{
    class JsonUtils
    {
        public static JObject FromObjectToJObject(object obj) {
            string jsonString = JsonConvert.SerializeObject(obj);
            return JObject.Parse(jsonString);
        }
    }
}
