﻿using Pokedex.Src.Database;
using Pokedex.Src.Model.Pokemon;
using Pokedex.Src.Model.Pokemon.Type;
using System.Collections.Generic;
using Pokedex.Src.Model.PokemonTable;

namespace Pokedex.Src.Service
{
    public class PokemonService
    {
        private readonly static string LANGUAGE_CONDITION = $"local_language_id = '{Settings.DEFAULT_LOCAL_LANGUAGE_ID}'";

        private readonly static string VERSION_GROUP_CONDITION = $"version_group_id = '{Settings.DEFAULT_VERSION_GROUP_ID}'";

        public static List<Pokemon> GetPokemons(bool withOverview = false) {
            List<Pokemon> result = new List<Pokemon>();
            DatabaseManager.ForEachSelectQuery<PokemonIdentifierTable>(
                $"select pokemon_species_id, name from pokemon_species_names where {PokemonService.LANGUAGE_CONDITION} order by pokemon_species_id",
                pokemonIdentifierTable => {
                    result.Add(PokemonService.GetPokemon(pokemonIdentifierTable.pokemon_species_id, pokemonIdentifierTable.name, withOverview));
                }
            );
            return result;
        }

        public static Pokemon GetPokemon(string pokemonId, string pokemonName, bool withOverview = false) {
            return new Pokemon(
                ushort.Parse(pokemonId),
                pokemonName,
                PokemonService.GeneratePokemonTypes(pokemonId),
                withOverview ? GeneratePokemonOverview(pokemonId) : null
            );
        }

        private static PokemonTypes GeneratePokemonTypes(string pokemonId) {
            List<TYPE> typeList = new List<TYPE>();
            DatabaseManager.ForEachSelectQuery<PokemonTypeTable>(
                $"select type_id from pokemon_types where pokemon_id = '{pokemonId}' order by slot",
                pokemonTypeTable => {
                    typeList.Add((TYPE) pokemonTypeTable.type_id);
                }
            );
            return new PokemonTypes(typeList);
        }

        private static PokemonOverview GeneratePokemonOverview(string pokemonId) {
            return new PokemonOverview(
                PokemonService.GeneratePokemonExperience(pokemonId),
                PokemonService.GeneratePokemonStatistics(pokemonId),
                PokemonService.GeneratePokemonSkills(pokemonId)
            );
        }

        private static Experience GeneratePokemonExperience(string pokemonId) {
            PokemonExperienceTable pokemonExperianceTable = DatabaseManager.GetFirstSelectQuery<PokemonExperienceTable>($"select base_experience from pokemon where id = {pokemonId}");
            return new Experience(pokemonExperianceTable?.base_experience);
        }

        private static Statistics GeneratePokemonStatistics(string pokemonId) {
            Dictionary<STATISTIC, ushort?> baseStatistics = new Dictionary<STATISTIC, ushort?>();
            Dictionary<STATISTIC, ushort?> efforts = new Dictionary<STATISTIC, ushort?>();
            DatabaseManager.ForEachSelectQuery<PokemonStatTable>(
                $"select stat_id, base_stat, effort from pokemon_stats where pokemon_id = {pokemonId}",
                pokemonStatTable => {
                    STATISTIC statistic = (STATISTIC) (pokemonStatTable.stat_id - 1);
                    baseStatistics.Add(statistic, pokemonStatTable?.base_stat);
                    efforts.Add(statistic, pokemonStatTable?.effort);
                }
            );
            return new Statistics(baseStatistics, efforts);
        }

        private static Skills GeneratePokemonSkills(string pokemonId) {
            List<LearnableMove> learnableMoves = new List<LearnableMove>();
            DatabaseManager.ForEachSelectQuery<LearnableMoveTable>(
                $"select * from moves, pokemon_moves, move_names where" +
                    $" moves.id = pokemon_moves.move_id" +
                    $" and moves.id = move_names.move_id" +
                    $" and level != 0" +
                    $" and pokemon_id = {pokemonId}" +
                    $" and {VERSION_GROUP_CONDITION}" +
                    $" and {LANGUAGE_CONDITION} order by level",
                learnableMoveTable => {
                    learnableMoves.Add(new LearnableMove(
                        learnableMoveTable?.level,
                        new Move(
                            learnableMoveTable?.name,
                            (TYPE) learnableMoveTable?.type_id,
                            (MOVE_CATEGORY) learnableMoveTable?.damage_class_id,
                            learnableMoveTable?.power == "0" ? "-" : learnableMoveTable?.power,
                            learnableMoveTable?.accuracy ?? "-",
                            learnableMoveTable?.pp
                        )
                    ));
                }
            );
            return new Skills(learnableMoves);
        }
    }
}
