﻿using Pokedex.Src.Model.Pokemon.Type;
using System;
using System.Globalization;
using Xamarin.Forms;

namespace Pokedex.Src.ValueConverters
{
    class TypeToImageSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value.GetType().Equals(typeof(TYPE)) && (TYPE) value != TYPE.UNKNOWN)
                return GetTypeImageResource((TYPE) value);
            return ImageSource.FromResource(""); ;
        }

        private ImageSource GetTypeImageResource(TYPE type) {
            return ImageSource.FromResource($"Pokedex.Resources.Types.{type.ToString()}.png");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
