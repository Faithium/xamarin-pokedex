﻿using Pokedex.Src.Model.Pokemon;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace Pokedex.Src.ValueConverters
{
    class MoveCategoryToImageSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value.GetType().Equals(typeof(MOVE_CATEGORY)))
                return GetTypeImageResource((MOVE_CATEGORY) value);
            return null;
        }

        private ImageSource GetTypeImageResource(MOVE_CATEGORY moveCategory) {
            return ImageSource.FromResource($"Pokedex.Resources.MoveCategories.{moveCategory.ToString()}.png");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
