﻿using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Pokedex.Src.Database
{
    public abstract class DatabaseConnectionCreator
    {
        public virtual SQLiteConnection CreateConnection() {
            var path = GetPathToDatabase();

            if (!File.Exists(path)) {
                CopyPreCreatedDatabase(path);
            }
            var conn = new SQLiteConnection(path);

            return conn;
        }

        protected string GetPersonalFolder() {
            return Environment.GetFolderPath(Environment.SpecialFolder.Personal);
        }

        protected abstract string GetPathToDatabase();

        protected abstract void CopyPreCreatedDatabase(string path);
    }
}
