﻿using SQLite;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Pokedex.Src.Database
{
    class DatabaseManager
    {
        private static SQLiteConnection dbConnection = DependencyService.Get<DatabaseConnectionCreator>().CreateConnection();
        
        public static List<T> SelectQuery<T>(string query, params object[] args) where T : new() {
            return dbConnection.Query<T>(query, args);
        }

        public static T GetFirstSelectQuery<T>(string query, params object[] args) where T : new() {
            List<T> queryResult = SelectQuery<T>(query, args);
            if (queryResult.Count > 0)
                return queryResult[0];
            return default(T);
        }

        public static void ForEachSelectQuery<T>(string query, Action<T> action, params object[] args) where T : new() {
            List<T> queryResults = SelectQuery<T>(query, args);
            queryResults.ForEach(queryResult => {
                action(queryResult);
            });
        }
    }
}
