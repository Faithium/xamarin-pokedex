﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pokedex.Src.Database
{
    public class Settings
    {
        public static string DATABASE_FILE_NAME = "pokedex";

        public static string DATABASE_FILE_TYPE = "sqlite";

        public static string DATABASE_FILE_FULL_NAME = $"{Settings.DATABASE_FILE_NAME}.{Settings.DATABASE_FILE_TYPE}";
    }
}
