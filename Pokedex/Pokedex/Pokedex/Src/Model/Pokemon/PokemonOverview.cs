﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pokedex.Src.Model.Pokemon
{
    public class PokemonOverview
    {
        public Experience Experience { get; }

        public Statistics Statistics { get; }

        public Skills Skills { get; }

        public PokemonOverview(Experience experience, Statistics statistics, Skills skills) {
            this.Experience = experience;
            this.Statistics = statistics;
            this.Skills = skills;
        }
    }
}
