﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pokedex.Src.Model.Pokemon
{
    public class Experience
    {
        public ushort? BaseExp { get; }

        public Experience(ushort? baseExp) {
            this.BaseExp = baseExp;
        }
    }
}
