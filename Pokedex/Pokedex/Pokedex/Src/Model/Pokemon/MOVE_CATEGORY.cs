﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pokedex.Src.Model.Pokemon
{
    public enum MOVE_CATEGORY
    {
        UNKNOWN,
        STATUS,
        PHYSICAL,
        SPECIAL
    }
}
