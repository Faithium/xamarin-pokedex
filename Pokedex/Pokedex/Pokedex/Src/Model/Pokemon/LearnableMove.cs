﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pokedex.Src.Model.Pokemon
{
    public class LearnableMove
    {
        public int? Level { get; }

        public Move Move { get; }

        public LearnableMove(int? level, Move move) {
            this.Level = level;
            this.Move = move;
        }
    }
}
