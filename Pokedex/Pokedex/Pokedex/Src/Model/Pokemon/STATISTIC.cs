﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pokedex.Src.Model.Pokemon
{
    public enum STATISTIC
    {
        HP,
        ATTACK,
        DEFENSE,
        SPE_ATTACK,
        SPE_DEFENSE,
        SPEED
    }
}
