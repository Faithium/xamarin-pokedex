﻿using Pokedex.Src.Model.Pokemon.Type;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pokedex.Src.Model.Pokemon
{
    public class Pokemon
    {
        public ushort Id { get; }

        public string Number { get { return $"#{Id}"; } }

        public string Name { get; }

        public PokemonTypes PokemonTypes { get; }

        public Appearance Appearance { get; }

        public PokemonOverview Overview { get; }

        public Pokemon(ushort id, string name, PokemonTypes pokemonTypes, PokemonOverview overview) {
            this.Id = id;
            this.Name = name;
            this.PokemonTypes = pokemonTypes;
            this.Appearance = new Appearance(id);
            this.Overview = overview;
        }
    }
}
