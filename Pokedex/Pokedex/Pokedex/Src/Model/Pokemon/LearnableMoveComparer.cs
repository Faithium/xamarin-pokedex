﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pokedex.Src.Model.Pokemon
{
    public class LearnableMoveComparer : Comparer<LearnableMove>
    {
        public override int Compare(LearnableMove x, LearnableMove y) {
            return Math.Sign(x.Level.Value.CompareTo(y.Level));
        }
    }
}
