﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Pokedex.Src.Model.Pokemon
{
    public class Statistics
    {
        private Dictionary<STATISTIC, ushort?> _baseStatistics { get; }

        public ushort? BaseHp { get { return this._baseStatistics[STATISTIC.HP]; } }
        public ushort? BaseAtt { get { return this._baseStatistics[STATISTIC.ATTACK]; } }
        public ushort? BaseDef { get { return this._baseStatistics[STATISTIC.DEFENSE]; } }
        public ushort? BaseSpeAtt { get { return this._baseStatistics[STATISTIC.SPE_ATTACK]; } }
        public ushort? BaseSpeDef { get { return this._baseStatistics[STATISTIC.SPE_DEFENSE]; } }
        public ushort? BaseSpeed { get { return this._baseStatistics[STATISTIC.SPEED]; } }

        private Dictionary<STATISTIC, ushort?> _efforts { get; }

        public ushort? EffortHp { get { return this._efforts[STATISTIC.HP]; } }
        public ushort? EffortAtt { get { return this._efforts[STATISTIC.ATTACK]; } }
        public ushort? EffortDef { get { return this._efforts[STATISTIC.DEFENSE]; } }
        public ushort? EffortSpeAtt { get { return this._efforts[STATISTIC.SPE_ATTACK]; } }
        public ushort? EffortSpeDef { get { return this._efforts[STATISTIC.SPE_DEFENSE]; } }
        public ushort? EffortSpeed { get { return this._efforts[STATISTIC.SPEED]; } }

        public Statistics(Dictionary<STATISTIC, ushort?> baseStatistics, Dictionary<STATISTIC, ushort?> efforts) {
            this._baseStatistics = baseStatistics;
            this._efforts = efforts;
        }
    }
}
