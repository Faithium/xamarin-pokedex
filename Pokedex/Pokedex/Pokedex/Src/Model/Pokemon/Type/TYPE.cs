﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pokedex.Src.Model.Pokemon.Type
{
    public enum TYPE
    {
        UNKNOWN,
        NORMAL,
        FIGHTING,
        FLYING,
        POISON,
        GROUND,
        ROCK,
        BUG,
        GHOST,
        STEEL,
        FIRE,
        WATER,
        GRASS,
        ELECTRIC,
        PSYCHIC,
        ICE,
        DRAGON,
        DARK,
        FAIRY
    }
}
