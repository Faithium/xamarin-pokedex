using System;
using System.Collections.Generic;
using System.Text;

namespace Pokedex.Src.Model.Pokemon.Type
{

    /// <summary>
    /// Defines the <see cref="TYPE"/>s of a <see cref="Pokemon"/> and so the <see cref="Pokemon"/>'s weakness table
    /// </summary>
    public class PokemonTypes {

        public TYPE[] _types { get; } = new TYPE[2];

        public TYPE FirstType { get { return _types[0]; } }

        public TYPE SecondType { get { return _types[1]; } }

        public PokemonTypes(List<TYPE> types) {
            this.InitTypes(types);
        }

        private void InitTypes(List<TYPE> types) {
            for (int typeIndex = 0; typeIndex < this._types.Length && typeIndex < types.Count; typeIndex++)
                this._types[typeIndex] = types[typeIndex];
        }
    }
}
