﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Pokedex.Src.Model.Pokemon
{
    public class Appearance
    {
        public ImageSource Sprite { get; }

        public Appearance(ushort id) {
            this.Sprite = ImageSource.FromUri(new System.Uri($"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/{id}.png"));
        }
    }
}
