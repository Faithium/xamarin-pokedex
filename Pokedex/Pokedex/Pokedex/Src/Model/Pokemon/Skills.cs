﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Pokedex.Src.Model.Pokemon
{
    public class Skills
    {
        private List<LearnableMove> _learnableMoves { get; }

        public ObservableCollection<LearnableMove> LearnableMoves { get { return new ObservableCollection<LearnableMove>(this._learnableMoves); } }

        public Skills(List<LearnableMove> learnableMoves) {
            this._learnableMoves = learnableMoves;
            this._learnableMoves.Sort(new LearnableMoveComparer());
        }
    }
}
