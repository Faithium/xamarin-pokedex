﻿using Pokedex.Src.Model.Pokemon.Type;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pokedex.Src.Model.Pokemon
{
    public class Move
    {
        public string Name { get; }

        public TYPE Type { get; }

        public MOVE_CATEGORY Category { get; }

        public string Power { get; }

        public string Accuracy { get; }

        public string Pp { get; }

        public Move(string name, TYPE type, MOVE_CATEGORY category, string power, string accuracy, string pp) {
            this.Name = name;
            this.Type = type;
            this.Category = category;
            this.Power = power;
            this.Accuracy = accuracy;
            this.Pp = pp;
        }
    }
}
