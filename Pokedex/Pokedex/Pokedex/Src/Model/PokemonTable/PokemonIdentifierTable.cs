﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pokedex.Src.Model.PokemonTable
{
    public class PokemonIdentifierTable
    {
        public string pokemon_species_id { get; set; }

        public string name { get; set; }
    }
}
