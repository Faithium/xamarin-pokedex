﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pokedex.Src.Model.PokemonTable
{
    public class PokemonExperienceTable
    {
        public ushort base_experience { get; set; }
    }
}
