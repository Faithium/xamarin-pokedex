﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pokedex.Src.Model.PokemonTable
{
    public class PokemonStatTable
    {
        public int stat_id { get; set; }

        public ushort base_stat { get; set; }

        public ushort effort { get; set; }
    }
}
