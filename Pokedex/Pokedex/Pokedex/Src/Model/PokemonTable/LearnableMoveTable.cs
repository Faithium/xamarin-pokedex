﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pokedex.Src.Model.PokemonTable
{
    public class LearnableMoveTable
    {
        public int level { get; set; }

        public string name { get; set; }

        public int type_id { get; set; }

        public int damage_class_id { get; set; }

        public string power { get; set; }

        public string accuracy { get; set; }

        public string pp { get; set; }
    }
}
