﻿using Pokedex.Src.Model.Pokemon;
using Pokedex.Src.Service;
using Prism.Mvvm;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Pokedex.ViewModels
{
    public class PokemonListViewModel : BindableBase
    {
        private ObservableCollection<Pokemon> _pokemons = new ObservableCollection<Pokemon>();
        public ObservableCollection<Pokemon> Pokemons
        {
            get { return _pokemons; }
            set { SetProperty(ref _pokemons, value); }
        }
    }
}
