﻿using Pokedex.Src.Model.Pokemon;
using Pokedex.Src.Service;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Pokedex.ViewModels
{
	public class PokemonDetailViewModel : BindableBase
	{
        private Pokemon _pokemon;
        public Pokemon Pokemon
        {
            get { return _pokemon; }
            set { SetProperty(ref _pokemon, value); }
        }
    }
}
